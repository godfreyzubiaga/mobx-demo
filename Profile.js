import React from 'react'
import { observer, inject } from 'mobx-react'
import ProblemView from './ProblemView'

export default inject('personStore')(observer(props => {
  const person = props.personStore.person
  const personStore = props.personStore

  return (
    <div style={{ backgroundColor: 'lightblue' }}>
      <strong>{person.lastName}</strong>, {person.firstName} <br />
      score: {person.score}

      <button onClick={person.gainSmallScore}>gain small score</button>
      <button onClick={person.gainHugeScore}>gain huge score</button>

      <h3>ze problems of {person.firstName}</h3>
      <ul>
        {
          person.problems.length == 0
          ? (
            <h2><em>No problems man!</em></h2>
          )
          :
          person.problems.map(prob => {
            <ProblemView key={prob._id} problem={prob}/>
          })
        }
      </ul>

      desc: 
        <input type="text" name="description" 
          onChange={personStore.changeField} 
          value={personStore.currentProblem.description} /><br />
      severity: 
        <input type="text" name="severity" 
          onChange={personStore.changeField} 
          value={personStore.currentProblem.severity} />
      <button onClick={personStore.addProblem}>add that problem</button>
    </div>
  )
}))