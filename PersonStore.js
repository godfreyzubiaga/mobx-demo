import { observable, action } from 'mobx'
import Person from './Person'
import Problem from './Problem';

export default class PersonStore {
  @observable person = new Person({ lastName: 'Coo', firstName: 'Mike', score: 10 })
  @observable currentProblem = new Problem({ description: '', severity: 0, _id: Math.random() })

  @action.bound
  changeField(event) {
    const { name, value } = event.target
    // name == descrioption or severity
    this.currentProblem[name] = value
  }

  @action.bound
  addProblem() {
    this.person.addProblem(this.currentProblem)
    this.currentProblem = new Problem({ description: '', severity: 0, _id: Math.random() })
  }
}