import React, { Component } from 'react';

class Dog {
  constructor(props) {
    Object.assign(this, props)
  }

  grow() {
    this.height += 7
  }
}

// class DogView extends Component {
class DogView extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      dog: new Dog({ name: 'Mike', height: 9 })
    }
  }

  // stage 3 -> use with caution
  // this will work on angular, but not redux
  handleGrowClick = () => {
    const dogClone = new Dog({ ...this.state.dog })
    dogClone.grow()
    // this.state.dog.grow()
    this.setState({ dog: this.state.dog }) // this causes the component rerender everytime
  }

  // overriden to control when the app rerenders
  // shouldComponentUpdate(nextProps, nextState) {
    // return nextState.dog.height > 50; // Component
    // return oldState !== newState; // PureComponent
  // }

  render() {
    return (
      <div>
        <strong>{this.state.dog.name}</strong>
        <em>{this.state.dog.height}</em>
        <button onClick={this.handleGrowClick}>GROW</button>
      </div>
    )
  }
}